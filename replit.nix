{ pkgs }: {
	deps = [
		pkgs.llvm_11
    pkgs.cargo
    pkgs.rustc
    pkgs.libffi
	];
}