mod bf_eval;
mod bf_parser;
mod lang_comp;
mod lang_parser;
mod linux_syscall;
mod types;
use crate::lang_parser::{LangParser, Rule, UserData};
use lang_comp::gen_code;
use pest_consume::Parser;

fn main() {
    let data = UserData::default();
    let mut parsed = LangParser::parse_with_userdata(
        Rule::Program,
        "
        out 78
        ",
        &data,
    )
    .unwrap();
    let result = LangParser::Program(parsed.next().unwrap());
    println!("{:?}", result);
    let _path = gen_code(result.unwrap());
    println!("{:?}", data);
}
