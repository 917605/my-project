use crate::lang_parser::Rule;
#[derive(Clone, Eq, PartialEq, Debug, Hash)]
pub enum Type {
    Byte,
    Arr { type_of: Box<Type>, size: usize },
}
#[derive(Clone, Eq, PartialEq, Debug, Hash)]
pub struct TypeError {
    found: Type,
    expected: Option<Type>,
    op: Rule,
}
use std::fmt;
impl fmt::Display for TypeError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        if let Some(t) = &self.expected {
            write!(
                f,
                "Found type {:?} expected type {:?} for {:?}",
                self.found, t, self.op
            )
        } else {
            write!(f, "Found invalid type {:?} for {:?}", self.found, self.op)
        }
    }
}
impl std::error::Error for TypeError {}
impl Type {
    pub fn binop_type(self, other: Self, op: Rule) -> Result<Type, TypeError> {
        match op {
            Rule::Add | Rule::Sub | Rule::Mul | Rule::Div | Rule::Mod => {
                if self == other {
                    Ok(self)
                } else {
                    Err(TypeError {
                        found: other,
                        expected: Some(self),
                        op,
                    })
                }
            }
            _ => panic!("Not a Binop: {:?}", op),
        }
    }
    pub fn unop_type(self, op: Rule) -> Result<Type, TypeError> {
        match op {
            Rule::Inc | Rule::Dec => match &self {
                Type::Byte => Ok(self),
                _ => Err(TypeError {
                    found: self,
                    expected: None,
                    op,
                }),
            },
            _ => panic!("Not a unop: {:?}", op),
        }
    }
}
