use crate::lang_comp::{Mir, MirOp};
use pest_consume::match_nodes;
use pest_consume::Error;
use pest_consume::Parser;
use std::collections::HashMap;
use std::iter::once;
#[derive(Parser)]
#[grammar_inline = r#"
Program = { SOI ~ Term ~ EOI }
Ident = @{ !Keyword ~!NumLit ~ (ASCII_ALPHA | "_") ~ (ASCII_ALPHANUMERIC | "_")* }
Keyword = _{ Loop | Let | In | Out }
    Loop = _{ "loop" }
    Let = _{ "let" }
    Out = _{ "out" }
    In = { "in" }
Block = { "{" ~ Stmt* ~ "}" }
Term = _{ Stmt+ }
LetStmt = {Let ~ Ident ~ "=" ~ Expr }
SetStmt = { Ident ~ "=" ~ Expr }
OutStmt = { Out ~ Expr }
Expr = { (Postfix | ExprTerm) ~ (BinOp ~ (Postfix | ExprTerm))*}
ExprTerm = _{ Prefix | NumLit | Ident | Paren | In }
Prefix = { (Dec | Inc)+ ~ ExprTerm }
Postfix = { ExprTerm ~ (Dec | Inc)+ }
    Dec = @{ "--" }
    Inc = @{ "++" }
BinOp = _{ Add | Sub | Div | Mod | Mul }
    Add = { "+" }
    Sub = { "-" }
    Mul = { "*" }
    Div = { "/" }
    Mod = { "%" }
Paren = { "(" ~ Expr ~ ")" }
NumLit = @{ ASCII_DIGIT ~ ASCII_DIGIT* }
Stmt = { LoopStmt | LetStmt | SetStmt | OutStmt | Block }
LoopStmt = { Loop ~ Expr ~ Block }
WHITESPACE = _{ " " | "\n" | "\t" | "\r\n" }
"#]
pub struct LangParser;
#[allow(unused)]
type Result<T> = std::result::Result<T, Error<Rule>>;
#[allow(unused)]
type Node<'i, 'u> = pest_consume::Node<'i, Rule, &'u UserData>;

#[pest_consume::parser]
impl LangParser {
    pub fn EOI(input: Node<'_, '_>) -> Result<()> {
        Ok(())
    }
    pub fn Program<'i>(input: Node<'i, '_>) -> Result<Vec<Mir<'i>>> {
        let mut res: Vec<_> = match_nodes!(input.children();
            [Stmt(mir).., EOI(_)]=> mir.flat_map(|n|n.into_iter()).collect(),
        );
        let data = input.user_data();
        data.vars
            .borrow_mut()
            .drain()
            .for_each(|(_, var)| res.push(Mir::new(MirOp::HeapDrop(var), input.as_span())));
        Ok(res)
    }
    pub fn Stmt<'i>(input: Node<'i, '_>) -> Result<Vec<Mir<'i>>> {
        Ok(match_nodes!(input.into_children();
        [Block(mir)]=>mir,
        [LetStmt(mir)]=>mir,
        [SetStmt(mir)]=>mir,
        [OutStmt(mir)]=>mir,
        [LoopStmt(mir)]=>mir,
        ))
    }
    pub fn Block<'i>(input: Node<'i, '_>) -> Result<Vec<Mir<'i>>> {
        let mut depth = input.user_data().depth.borrow_mut();
        depth.0 += 1;
        drop(depth);
        let mut res: Result<Vec<_>> = Ok(match_nodes!(input.children();
            [Stmt(mir)..]=>mir.flat_map(|n|n.into_iter()).collect(),
        ));
        let mut depth = input.user_data().depth.borrow_mut();
        let mut vars = input.user_data().vars.borrow_mut();
        let mut new_map = HashMap::new();
        new_map.extend(vars.drain().filter(|((_, n), var)| {
            let eq = n.0 == depth.0;
            if eq {
                res.iter_mut()
                    .for_each(|mir| mir.push(Mir::new(MirOp::HeapDrop(*var), input.as_span())));
            }
            !eq
        }));
        depth.0 -= 1;
        *vars = new_map;
        res
    }
    pub fn LetStmt<'i>(input: Node<'i, '_>) -> Result<Vec<Mir<'i>>> {
        let data = input.user_data();
        Ok(match_nodes!(input.children();
        [Ident(string), Expr(mut mir)]=>{
            let mut var = data.var.borrow_mut();
            *var += 1;
            let mut map = data.vars.borrow_mut();
            let scope = data.depth.borrow();
            let key = (string,*scope);
            if map.contains_key(&key){
                return Err(input.error("Duplicate local variable"));
            }
            map.insert(key, *var);
            mir.push(Mir::new(MirOp::HeapInit(*var),input.as_span()));
            mir
        },
        ))
    }
    pub fn Expr<'i>(input: Node<'i, '_>) -> Result<Vec<Mir<'i>>> {
        let data = input.user_data();
        return expr_climber(data, input.into_children().into_pairs());
    }
    pub fn Ident(input: Node<'_, '_>) -> Result<String> {
        Ok(input.as_str().to_string())
    }
    pub fn SetStmt<'i>(input: Node<'i, '_>) -> Result<Vec<Mir<'i>>> {
        let data = input.user_data();
        Ok(match_nodes!(input.children();
        [Ident(string), Expr(mut mir)]=>{
            if let Some(key) = find_var(data, string) {
                let map = data.vars.borrow();
                let res = map.get(&key).unwrap();
                mir.push(Mir::new(MirOp::HeapStore(*res),input.as_span()));
                mir
            } else{
                return Err(input.error("Uninitialized variable"))
            }
        },
        ))
    }
    pub fn OutStmt<'i>(input: Node<'i, '_>) -> Result<Vec<Mir<'i>>> {
        Ok(match_nodes!(input.children();
            [Expr(mut mir)]=>{
                mir.push(Mir::new(MirOp::Output,input.as_span()));
                mir
            }
        ))
    }
    pub fn LoopStmt<'i>(input: Node<'i, '_>) -> Result<Vec<Mir<'i>>> {
        Ok(match_nodes!(input.children();
            [Expr(expr_mir), Block(mir)]=>{
                let data = input.user_data();
                let mut lbl = data.lbl.borrow_mut();
                *lbl += 1;
                once(Mir::new(MirOp::Lbl(*lbl),input.as_span()))
                    .chain(mir)
                    .chain(expr_mir)
                    .chain(once(Mir::new(MirOp::Jnz(*lbl),input.as_span())))
                    .collect()
            },
        ))
    }
}

fn find_var(data: &UserData, s: String) -> Option<(String, Depth)> {
    let map = data.vars.borrow();
    let scope = data.depth.borrow();
    let mut found = false;
    let mut key = (s, *scope);
    for _ in 0..=scope.0 {
        if map.contains_key(&key) {
            found = true;
            break;
        }
        key.1 .0 = key.1 .0.checked_sub(1)?;
    }
    if found {
        Some(key)
    } else {
        None
    }
}
use lazy_static::lazy_static;
use pest::prec_climber::Assoc::*;
use pest::prec_climber::Operator;
use pest::prec_climber::PrecClimber;
lazy_static! {
    static ref PREC_CLIMBER: PrecClimber<Rule> = PrecClimber::new(vec![
        Operator::new(Rule::Sub, Left) | Operator::new(Rule::Add, Left),
        Operator::new(Rule::Mul, Left)
            | Operator::new(Rule::Div, Left)
            | Operator::new(Rule::Mod, Left)
    ]);
}
use pest::error::ErrorVariant;
use pest::iterators::Pair;
fn expr_climber<'i, I: Iterator<Item = Pair<'i, Rule>>>(
    data: &UserData,
    input: I,
) -> Result<Vec<Mir<'i>>> {
    PREC_CLIMBER.climb(
        input,
        |pair| -> Result<_> {
            match &pair.as_rule() {
                Rule::Ident => {
                    let name = pair.as_str().to_owned();
                    if let Some(found) = find_var(data, name) {
                        let map = data.vars.borrow();
                        return Ok(vec![Mir::new(
                            MirOp::HeapLoad(*map.get(&found).unwrap()),
                            pair.as_span(),
                        )]);
                    } else {
                        return Err(Error::new_from_span(
                            error("Uninitialized variable"),
                            pair.as_span(),
                        ));
                    }
                }
                Rule::NumLit => {
                    let res = pair.as_str().parse::<u8>();
                    return res
                        .map(|n| vec![Mir::new(MirOp::Push(n), pair.as_span())])
                        .map_err(|e| Error::new_from_span(error(e), pair.as_span()));
                }
                Rule::In => Ok(vec![Mir::new(MirOp::Input, pair.as_span())]),
                Rule::Paren => expr_climber(data, pair.into_inner()),
                Rule::Postfix => {
                    let mut inner = pair.clone().into_inner();
                    let mut res = expr_climber(data, inner.next().into_iter())?;
                    for op in inner {
                        match op.as_rule() {
                            Rule::Inc => res.push(Mir::new(MirOp::Inc, pair.as_span())),
                            Rule::Dec => res.push(Mir::new(MirOp::Dec, pair.as_span())),
                            _ => unreachable!(),
                        }
                    }
                    Ok(res)
                }
                Rule::Prefix => {
                    let mut inner = pair.clone().into_inner().rev();
                    let mut res = expr_climber(data, inner.next().into_iter())?;
                    for op in inner {
                        match op.as_rule() {
                            Rule::Inc => res.push(Mir::new(MirOp::Inc, pair.as_span())),
                            Rule::Dec => res.push(Mir::new(MirOp::Dec, pair.as_span())),
                            _ => unreachable!(),
                        }
                    }
                    Ok(res)
                }
                Rule::Expr => expr_climber(data, pair.into_inner()),
                _ => unreachable!(),
            }
        },
        |left, op, right| {
            let mut left = left?;
            let mut right = right?;
            left.append(&mut right);
            match op.as_rule() {
                Rule::Add => left.push(Mir::new(MirOp::Add, op.as_span())),
                Rule::Sub => left.push(Mir::new(MirOp::Sub, op.as_span())),
                Rule::Mul => left.push(Mir::new(MirOp::Mul, op.as_span())),
                Rule::Div => left.push(Mir::new(MirOp::Div, op.as_span())),
                Rule::Mod => left.push(Mir::new(MirOp::Mod, op.as_span())),
                _ => unreachable!(),
            }
            Ok(left)
        },
    )
}

fn error<S: ToString>(s: S) -> ErrorVariant<Rule> {
    ErrorVariant::CustomError {
        message: s.to_string(),
    }
}
use std::cell::RefCell;
#[derive(Clone, Eq, PartialEq)]
pub struct UserData {
    lbl: RefCell<usize>,
    vars: RefCell<HashMap<(String, Depth), usize>>,
    var: RefCell<usize>,
    depth: RefCell<Depth>,
}
use std::fmt::{self, Debug, Formatter};
impl Debug for UserData {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        f.debug_struct("UserData")
            .field("lbl", &*self.lbl.borrow())
            .field("vars", &*self.vars.borrow())
            .field("var", &*self.var.borrow())
            .field("depth", &*self.depth.borrow())
            .finish()
    }
}
impl Default for UserData {
    fn default() -> Self {
        UserData {
            lbl: RefCell::new(1),
            vars: Default::default(),
            var: Default::default(),
            depth: Default::default(),
        }
    }
}

#[derive(Clone, Copy, PartialEq, Eq, Ord, PartialOrd, Default, Hash, Debug)]
struct Depth(usize);
