#[derive(Debug, Clone, Copy, Eq, PartialEq, Hash)]
pub enum MirOp {
    HeapInit(usize),
    HeapDrop(usize),
    HeapLoad(usize),
    HeapStore(usize),
    Push(u8),
    Add,
    Sub,
    Mul,
    Div,
    Mod,
    Inc,
    Dec,
    Output,
    Input,
    Lbl(usize),
    Jnz(usize),
}
#[derive(Debug, Clone, Eq, PartialEq, Hash)]
pub struct Mir<'i> {
    op: MirOp,
    span: Span<'i>,
}
impl Mir<'_> {
    pub fn as_op(&self) -> MirOp {
        self.op
    }
    pub fn as_span(&self) -> Span<'_> {
        self.span.clone()
    }
    pub fn new<'i>(op: MirOp, span: Span<'i>) -> Mir<'i> {
        Mir { op, span }
    }
}
use crate::linux_syscall::*;
use inkwell::context::Context;
use inkwell::targets::{CodeModel, RelocMode};
use inkwell::targets::{FileType, Target, TargetMachine};
use inkwell::values::{BasicValueEnum, IntValue};
use inkwell::OptimizationLevel;
use pest::Span;
use std::collections::HashMap;
use std::convert::TryInto;
pub fn gen_code(input: Vec<Mir<'_>>) -> String {
    let context = Context::create();
    let module = context.create_module("code_gen");
    let i8_type = context.i8_type();
    let i64_type = context.i64_type();
    let void = context.void_type();
    let fn_type = void.fn_type(&mut vec![], false);
    let fn_name = module.add_function("main", fn_type, None);
    let basic_block = context.append_basic_block(fn_name, "entry");
    let builder = context.create_builder();
    builder.position_at_end(basic_block);
    let mut heap_map = HashMap::new();
    let mut bb_map = HashMap::new();
    let mut stack: Vec<BasicValueEnum> = vec![];
    let triple = TargetMachine::get_default_triple();
    let cpu = TargetMachine::get_host_cpu_name();
    let features = TargetMachine::get_host_cpu_features();
    Target::initialize_x86(&Default::default());
    let target = Target::from_triple(&triple);
    let machine = target
        .unwrap()
        .create_target_machine(
            &triple,
            &cpu.to_string(),
            &features.to_string(),
            OptimizationLevel::None,
            RelocMode::Default,
            CodeModel::Default,
        )
        .unwrap();
    gen_write_byte(&module, machine.get_target_data(), &context);
    gen_read_byte(&module, machine.get_target_data(), &context);
    for op in input {
        match op.as_op() {
            MirOp::HeapInit(n) => {
                let string = format!("heap_{}", n);
                let value = builder.build_alloca(i8_type, &string);
                let _value = builder
                    .build_store::<IntValue>(value, stack.pop().unwrap().try_into().unwrap());
                heap_map.insert(n, value);
            }
            MirOp::HeapDrop(n) => {
                heap_map.remove(&n);
            }
            MirOp::HeapLoad(n) => {
                let value =
                    builder.build_load(*heap_map.get(&n).unwrap(), &format!("heap_load_{}", n));
                stack.push(value);
            }
            MirOp::HeapStore(n) => {
                let _value = builder.build_store::<IntValue>(
                    *heap_map.get(&n).unwrap(),
                    stack.pop().unwrap().try_into().unwrap(),
                );
            }
            MirOp::Push(n) => {
                stack.push(i8_type.const_int(n as u64, false).into());
            }
            MirOp::Add => {
                let second = stack.pop().unwrap();
                let first = stack.pop().unwrap();
                let value: IntValue = builder.build_int_add(
                    first.try_into().unwrap(),
                    second.try_into().unwrap(),
                    "add_temp",
                );
                stack.push(value.into());
            }
            MirOp::Sub => {
                let second = stack.pop().unwrap();
                let first = stack.pop().unwrap();
                let value: IntValue = builder.build_int_sub(
                    first.try_into().unwrap(),
                    second.try_into().unwrap(),
                    "sub_temp",
                );
                stack.push(value.into());
            }
            MirOp::Inc => {
                let value: IntValue = builder.build_int_add(
                    stack.pop().unwrap().try_into().unwrap(),
                    i8_type.const_int(1, false),
                    "inc_temp",
                );
                stack.push(value.into());
            }
            MirOp::Dec => {
                let value: IntValue = builder.build_int_sub(
                    stack.pop().unwrap().try_into().unwrap(),
                    i8_type.const_int(1, false),
                    "dec_temp",
                );
                stack.push(value.into());
            }
            MirOp::Lbl(n) => {
                let lbl_block = context.append_basic_block(fn_name, &format!("label_{}", n));
                builder.build_unconditional_branch(lbl_block);
                builder.position_at_end(lbl_block);
                bb_map.insert(n, lbl_block);
            }
            MirOp::Jnz(n) => {
                let should_jump = builder.build_int_compare(
                    inkwell::IntPredicate::EQ,
                    stack.pop().unwrap().try_into().unwrap(),
                    i8_type.const_int(0, false),
                    "cmp_tmp",
                );
                let jmp_block = context.append_basic_block(fn_name, &format!("jmp_{}", n));
                builder.build_conditional_branch(should_jump, jmp_block, *bb_map.get(&n).unwrap());
                builder.position_at_end(jmp_block);
            }
            MirOp::Input => {
                let func = module.get_function("read_byte").unwrap();
                let val = builder
                    .build_call(func, &[], "input")
                    .try_as_basic_value()
                    .unwrap_left();
                stack.push(val);
            }
            MirOp::Output => {
                let func = module.get_function("write_bytes").unwrap();
                match stack.pop().unwrap() {
                    BasicValueEnum::IntValue(i) => {
                        let val = builder.build_alloca(i8_type, "print_alloca");
                        builder.build_store(val, i);
                        builder.build_call(
                            func,
                            &[
                                val.into(),
                                context
                                    .ptr_sized_int_type(&machine.get_target_data(), None)
                                    .const_int(1, false)
                                    .into(),
                            ],
                            "print_call",
                        );
                    }
                    BasicValueEnum::PointerValue(p) => {
                        builder.build_call(
                            func,
                            &[p.into(), i64_type.const_int(1, false).into()],
                            "print_call",
                        );
                    }
                    _ => unreachable!(),
                }
            }
            MirOp::Mul => {
                let a = stack.pop().unwrap().try_into().unwrap();
                let b = stack.pop().unwrap().try_into().unwrap();
                let val: IntValue = builder.build_int_mul(b, a, "mul_temp");
                stack.push(val.into());
            }
            MirOp::Div => {
                let a = stack.pop().unwrap().try_into().unwrap();
                let b = stack.pop().unwrap().try_into().unwrap();
                let val: IntValue = builder.build_int_unsigned_div(b, a, "div_temp");
                stack.push(val.into());
            }
            MirOp::Mod => {
                let a = stack.pop().unwrap().try_into().unwrap();
                let b = stack.pop().unwrap().try_into().unwrap();
                let val: IntValue = builder.build_int_unsigned_rem(b, a, "mod_temp");
                stack.push(val.into());
            }
        }
    }
    builder.build_return(None);
    let start = module.add_function("_start", fn_type, None);
    let bb = context.append_basic_block(start, "entry");
    builder.position_at_end(bb);
    builder.build_call(fn_name, &[], "start");
    let exit = gen_exit(&context);
    builder.build_call(
        exit,
        &[context.i32_type().const_int(0, false).into()],
        "exit",
    );
    builder.build_unreachable();
    fn_name.verify(true);
    println!(
        "{}",
        module.print_to_string().to_string().replace("//n", "/n")
    );
    let engine = module
        .create_jit_execution_engine(OptimizationLevel::None)
        .unwrap();
    unsafe {
        engine
            .get_function::<unsafe extern "C" fn()>("main")
            .unwrap()
            .call();
    }
    machine
        .write_to_file(&module, FileType::Assembly, "test.s".as_ref())
        .unwrap();
    "test.s".to_owned()
}
