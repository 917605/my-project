use pest_consume::Parser;
#[derive(Parser)]
#[grammar_inline = r#"
Program = _{ SOI ~ Term ~ EOI }
Op = _{ Inc | Dec | Out | In | Right | Left }
    Inc = { "+"+ }
    Dec = { "-"+ }
    Out = { "." }
    In = { "," }
    Right = { ">"+ }
    Left = { "<"+ }
While = { "[" ~ Term ~ "]" }
Term = _{ (While | Op)* }
WHITESPACE = _{ '\0'..' ' |  '!'..'*' | '/'..';' | "=" | '?'..'Z' | "\\" | '^'..'\u{10FFFF}' }
"#]
pub struct BFParser;
