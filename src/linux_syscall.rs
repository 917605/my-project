use inkwell::context::Context;
use inkwell::module::Module;
use inkwell::targets::TargetData;
use inkwell::values::{FunctionValue, PointerValue};
use inkwell::AddressSpace;
use inkwell::InlineAsmDialect;

pub fn gen_write_byte<'ctx: 'module, 'module>(
    module: &Module<'module>,
    data: TargetData,
    context: &'ctx Context,
) -> FunctionValue<'module> {
    let ptr_type = context.ptr_sized_int_type(&data, None);
    let i8_type = context.i8_type();
    let ptr = gen_write(context, data);
    let func = module.add_function(
        "write_bytes",
        context.void_type().fn_type(
            &[
                i8_type.ptr_type(AddressSpace::Generic).into(),
                ptr_type.into(),
            ],
            false,
        ),
        None,
    );
    let builder = context.create_builder();
    let bb = context.append_basic_block(func, "entry");
    builder.position_at_end(bb);
    builder.build_call(
        ptr,
        &[
            context.i32_type().const_int(1, false).into(),
            func.get_first_param().unwrap(),
            func.get_last_param().unwrap(),
        ],
        "write_call",
    );
    builder.build_return(None);
    func.verify(true);
    func
}

pub fn gen_read_byte<'ctx: 'module, 'module>(
    module: &Module<'module>,
    data: TargetData,
    context: &'ctx Context,
) -> FunctionValue<'module> {
    let ptr_type = context.ptr_sized_int_type(&data, None);
    let i8_type = context.i8_type();
    let ptr = gen_read(context, data);
    let func = module.add_function("read_byte", i8_type.fn_type(&[], false), None);
    let builder = context.create_builder();
    let bb = context.append_basic_block(func, "entry");
    builder.position_at_end(bb);
    let ptr_val = builder.build_alloca(i8_type, "read_alloca");
    builder.build_call(
        ptr,
        &[
            context.i32_type().const_int(1, false).into(),
            ptr_val.into(),
            ptr_type.const_int(1, false).into(),
        ],
        "read_call",
    );
    let load = builder.build_load(ptr_val, "read_load");
    builder.build_return(Some(&load));
    func.verify(true);
    func
}
pub fn gen_read(context: &Context, data: TargetData) -> PointerValue<'_> {
    let ptr_type = context.ptr_sized_int_type(&data, None);
    context.create_inline_asm(
        ptr_type.fn_type(
            &[
                context.i32_type().into(),
                context.i8_type().ptr_type(AddressSpace::Generic).into(),
                ptr_type.into(),
            ],
            false,
        ),
        "movq $$0, %rax ${:comment} read
        syscall"
            .into(),
        "={rcx},~{r11},~{rax},~{memory},{rdi},{rsi},{rdx}".into(),
        true,
        true,
        Some(InlineAsmDialect::ATT),
    )
}
pub fn gen_write(context: &Context, data: TargetData) -> PointerValue<'_> {
    let ptr_type = context.ptr_sized_int_type(&data, None);
    context.create_inline_asm(
        ptr_type.fn_type(
            &[
                context.i32_type().into(),
                context.i8_type().ptr_type(AddressSpace::Generic).into(),
                ptr_type.into(),
            ],
            false,
        ),
        "movq $$1, %rax ${:comment} write
        syscall"
            .into(),
        "={rcx},~{r11},~{rax},~{memory},{rdi},{rsi},{rdx}".into(),
        true,
        true,
        Some(InlineAsmDialect::ATT),
    )
}
pub fn gen_exit(context: &Context) -> PointerValue<'_> {
    context.create_inline_asm(
        context
            .void_type()
            .fn_type(&[context.i32_type().into()], false),
        "movq $$60, %rax
        syscall
        "
        .into(),
        "~{rax},~{r11},{rdi}".into(),
        true,
        true,
        Some(InlineAsmDialect::ATT),
    )
}
