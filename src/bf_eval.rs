use crate::bf_parser::{
    BFParser,
    Rule::{self, *},
};
use pest::iterators::Pair;
use pest::Parser;
use std::io::prelude::*;
#[allow(unused)]
pub fn eval_bf<R: Read, W: Write>(code: &str, mut input: R, mut output: W) {
    let mut tape = [0u8; 30_000];
    let mut ptr = 0;
    eval_bf_helper(
        BFParser::parse(Rule::Program, code).unwrap_or_else(|_| panic!("Unmatched Bracket")),
        &mut ptr,
        &mut tape,
        &mut input,
        &mut output,
    )
}
fn eval_bf_helper<'i, I: Iterator<Item = Pair<'i, Rule>>, R: Read, W: Write>(
    code: I,
    ptr: &mut usize,
    tape: &mut [u8],
    input: &mut R,
    output: &mut W,
) {
    for i in code {
        match i.as_rule() {
            Inc => tape[*ptr] = tape[*ptr].wrapping_add(i.as_str().len() as u8),
            Dec => tape[*ptr] = tape[*ptr].wrapping_sub(i.as_str().len() as u8),
            Left => *ptr -= i.as_str().len(),
            Right => *ptr += i.as_str().len(),
            Out => write!(output, "{}", tape[*ptr] as char).expect("Failed the output call: "),
            In => match input.read(&mut tape[*ptr..=*ptr]) {
                Ok(0) => tape[*ptr] = 0,
                Ok(_) => (),
                Err(e) => panic!("Failed the read call: {}", e),
            },
            While => {
                while tape[*ptr] != 0 {
                    eval_bf_helper(i.clone().into_inner(), ptr, tape, input, output);
                }
            }
            _ => (),
        }
    }
}
